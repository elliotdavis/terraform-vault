# terraform-vault

![hashicorp vault](https://www.datocms-assets.com/2885/1506458374-blog-vault-vertical-new.svg "Hashicorp Vault")

Configuration for Lereta's Vault Instance will go here

Policy configuration, auth backend configurations, and eventually others will go here

## NOTE:
To implement changes, please select correct workspace: dev/prod
`terraform workspace select dev` or `terraform workspace select prod`

In addition you will also need to authenticate to appropriate vault cluster

`export VAULT_ADDR=https://vault.lereta.io/`
OR
`export VAULT_ADDR=https://vault-dev.lereta.io/`
then
`vault login`

### Also in order to run `terraform apply` in this repo, you'll need to download the Base64 Encoded JWT for vaultadmin Service Account in Vault Path: secret/sre/vaultadmin-serviceaccount key=base64_encoded