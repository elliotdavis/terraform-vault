resource "vault_mount" "sysops_mount" {
  path        = "sysops"
  type        = "kv"
  description = "Sysops Secret Backend"

  options = {
    version = "2"
  }
}

resource "vault_mount" "sre_mount" {
  path        = "sre"
  type        = "kv"
  description = "SRE Secret Backend"

  options = {
    version = "2"
  }
}

resource "vault_mount" "terraform_mount" {
  path        = "terraform"
  type        = "kv"
  description = "Terraform Secret Backend"

  options = {
    version = "2"
  }
}

## Ensuring Test Secrets Exist
resource "vault_generic_secret" "secret_test" {
  path = "secret/test"

  data_json = <<EOF
{
  "jenkins": "huzzah",
  "test": "Hello World",
  "item3": "three"
}
EOF
}

resource "vault_mount" "eng_mount" {
  path        = "eng"
  type        = "kv"
  description = "Engineering Secret Backend"

  options = {
    version = "2"
  }
}
