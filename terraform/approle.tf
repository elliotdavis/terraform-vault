resource "vault_auth_backend" "approle" {
  type = "approle"
  path = "approle"
}

resource "vault_approle_auth_backend_role" "jenkins_role" {
  backend                = vault_auth_backend.approle.path
  role_name              = "jenkins-role-dev"
  token_policies         = [vault_policy.default.name, vault_policy.approle.name, vault_policy.github_auto_eng.name]
  token_explicit_max_ttl = "1800"
  secret_id_bound_cidrs  = var.jenkins_nat
  secret_id_ttl          = "0"
}

resource "vault_approle_auth_backend_role" "jenkins_role_prod" {
  backend                = vault_auth_backend.approle.path
  role_name              = "jenkins-role-prd"
  token_policies         = [vault_policy.default.name, vault_policy.approle.name, vault_policy.github_auto_eng.name]
  secret_id_bound_cidrs  = var.jenkins_nat
  token_explicit_max_ttl = "1800"
  secret_id_ttl          = "0"
}

resource "vault_approle_auth_backend_role" "salt_role" {
  backend               = vault_auth_backend.approle.path
  role_name             = "salt-role"
  token_policies        = [vault_policy.default.name, vault_policy.approle.name, vault_policy.auto_sec_vault.name]
  secret_id_bound_cidrs = var.salt_nat
  secret_id_ttl         = "0"
}
