path "postgres/creds/dev*" {
  capabilities = ["read"]
}

path "postgres/creds/tst*" {
  capabilities = ["read"]
}

path "postgres/creds/stg*" {
  capabilities = ["read"]
}

path "postgres/creds/prd*" {
  capabilities = ["read"]
}

path "postgres/creds/sys*" {
  capabilities = ["read"]
}

path "sys/revoke/postgres/creds/*" {
  capabilities = ["update"]
}

path "secret/data/service_accounts/*" {
  capabilities = ["read"]
}

# For Dependency Checker on Jenkins
path "secret/data/test" {
  capabilities = ["read"]
}
