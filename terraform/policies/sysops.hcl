path "sysops/" {
  capabilities = ["read", "list"]
}

path "sysops/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
