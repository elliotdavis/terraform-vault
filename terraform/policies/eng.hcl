# List, create, update, and delete key/value secrets
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Denying secret/ paths for SRE's Only
path "secret/+/service_accounts/" {
  capabilities = ["deny"]
}

path "secret/+/dns/" {
  capabilities = ["deny"]
}

path "secret/+/sre/" {
  capabilities = ["deny"]
}

path "secret/+/goldfish" {
  capabilities = ["deny"]
}

path "secret/+/test" {
  capabilities = ["deny"]
}

path "secret/+/cert-bot" {
  capabilities = ["deny"]
}

path "secret/+/databases/stg/" {
  capabilities = ["deny"]
}

path "secret/+/databases/prd/" {
  capabilities = ["deny"]
}

# Manage postgres database backend
path "postgres/creds/dev*" {
  capabilities = ["read"]
}

# encrypt  gcpkms  backend
path "gcpkms/encrypt/dev-*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

# decrypt  gcpkms  backend
path "gcpkms/decrypt/dev-*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "eng/*" {
  capabilities = ["create", "read", "update", "list"]
}

## Deny Destroy
path "eng/destroy/*" {
  capabilities = ["deny"]
}

path "secret/destroy/*" {
  capabilities = ["deny"]
}
