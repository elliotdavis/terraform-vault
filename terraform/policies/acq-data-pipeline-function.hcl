path "postgres/creds/dev-acq-data-pipeline-function" {
  capabilities = ["read"]
}

path "postgres/creds/tst-acq-data-pipeline-function" {
  capabilities = ["read"]
}

path "postgres/creds/stg-acq-data-pipeline-function" {
  capabilities = ["read"]
}

path "postgres/creds/prd-acq-data-pipeline-function" {
  capabilities = ["read"]
}

path "sys/revoke/postgres/creds/*" {
  capabilities = ["update"]
}
