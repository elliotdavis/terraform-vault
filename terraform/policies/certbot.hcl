# Login with AppRole
path "auth/approle/login" {
  capabilities = ["create", "read"]
}

# Read test data
# Set the path to "secret/data/mysql/*" if you are running `kv-v2`
path "secret/*" {
  capabilities = ["list"]
}

path "secret/dns/*" {
  capabilities = ["read", "list"]
}

path "secret/cert-bot/*" {
  capabilities = ["read", "list"]
}
