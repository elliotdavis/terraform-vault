path "terraform/*" {
  capabilities = ["read", "list"]
}

path "terraform/atlantis/*" {
  capabilities = ["read", "list"]
}

path "terraform/ssl/lereta_io/*" {
  capabilities = ["read", "list"]
}

path "terraform/ssl/lereta_net/*" {
  capabilities = ["read", "list"]
}

path "auth/token/create" {
  capabilities = ["create", "update"]
}
