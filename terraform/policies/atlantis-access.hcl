path "terraform/*" {
  capabilities = ["read", "list"]
}

path "terraform/atlantis/*" {
  capabilities = ["read", "list"]
}

path "auth/token/create" {
  capabilities = ["create", "update"]
}

path "gcp/key/atlantis*" {
  capabilities = ["read"]
}
