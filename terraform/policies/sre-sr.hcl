path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sysops/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sre/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "terraform/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "gcp/key/tfadmin-key" {
  capabilities = ["read"]
}

path "gcp/key/dev-key" {
  capabilities = ["read"]
}

path "gcp/key/sre-key" {
  capabilities = ["read"]
}

path "gcp/key/tst-key" {
  capabilities = ["read"]
}

path "gcp/key/stg-key" {
  capabilities = ["read"]
}

path "gcp/key/prd-key" {
  capabilities = ["read"]
}

path "postgres/*" {
  capabilities = ["read"]
}

path "sys/revoke/postgres/creds/*" {
  capabilities = ["update"]
}

path "sys/revoke/*" {
  capabilities = ["update", "list", "sudo"]
}

path "sys/leases/*" {
  capabilities = ["update", "list", "sudo"]
}
