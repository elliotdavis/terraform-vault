path "terraform/*" {
  capabilities = ["read"]
}

path "postgres/creds/dev*" {
  capabilities = ["read"]
}

path "postgres/creds/tst*" {
  capabilities = ["read"]
}

path "auth/token/lookup-self" {
  capabilities = ["read"]
}

# For Dependency Checker on Jenkins
path "secret/test" {
  capabilities = ["read"]
}
