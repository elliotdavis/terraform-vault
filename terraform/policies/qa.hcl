# List, create, update, and delete key/value secrets
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Denying secret/ paths for SRE's Only
path "secret/+/service_accounts/" {
  capabilities = ["deny"]
}

path "secret/+/dns/" {
  capabilities = ["deny"]
}

path "secret/+/sre/" {
  capabilities = ["deny"]
}

path "secret/+/goldfish" {
  capabilities = ["deny"]
}

path "secret/+/test" {
  capabilities = ["deny"]
}

path "secret/+/cert-bot" {
  capabilities = ["deny"]
}

path "secret/+/databases/stg/" {
  capabilities = ["deny"]
}

path "secret/+/databases/prd/" {
  capabilities = ["deny"]
}

path "secret/+/databases/dev/dev_user" {
  capabilities = ["deny"]
}
