# Manage auth methods broadly across Vault
path "auth/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create, update, and delete auth methods
path "sys/auth/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List auth methods
path "sys/auth" {
  capabilities = ["read"]
}

# List existing policies via CLI
path "sys/policy" {
  capabilities = ["read"]
}

# Create and manage ACL policies via CLI
path "sys/policy/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create and manage ACL policies via API
path "sys/policies/acl/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage secret engines broadly across Vault
path "sys/mounts/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List existing secret engines
path "sys/mounts" {
  capabilities = ["read"]
}

# Read health checks
path "sys/health" {
  capabilities = ["read", "sudo"]
}

path "sys/revoke/*" {
  capabilities = ["update"]
}

path "sys/leases/*" {
  capabilities = ["update", "list"]
}

# List, create, update, and delete key/value secrets
path "terraform/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete key/value secrets /kv path
path "kv/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage postgres database backend
path "postgres/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage  gcpkms  backend
path "gcpkms/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage GCP Roles (Roleset/Backends)
path "gcp/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete key/value secrets /kv path
path "sysops/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sre/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "eng/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
