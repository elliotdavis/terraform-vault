# List, create, update, and delete key/value secrets
path "sre/*" {
  capabilities = ["read", "list"]
}

path "secret/*" {
  capabilities = ["read", "list"]
}

path "terraform/*" {
  capabilities = ["read", "list"]
}
