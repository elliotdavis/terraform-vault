path "terraform/*" {
  capabilities = ["read"]
}

path "secrets/*" {
  capabilities = ["read"]
}

path "auth/token/lookup-self" {
  capabilities = ["read"]
}
