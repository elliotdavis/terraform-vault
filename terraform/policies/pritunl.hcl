path "terraform/*" {
  capabilities = ["read", "list"]
}

path "terraform/pritunl/*" {
  capabilities = ["read", "list"]
}

path "auth/token/create" {
  capabilities = ["create", "update"]
}
