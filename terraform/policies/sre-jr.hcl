path "secret/*" {
  capabilities = ["read", "list"]
}

path "sysops/*" {
  capabilities = ["read", "list"]
}

path "sre/*" {
  capabilities = ["read", "list"]
}

path "terraform/*" {
  capabilities = ["read", "list"]
}

path "gcp/key/dev-key" {
  capabilities = ["read"]
}

path "gcp/key/sre-key" {
  capabilities = ["read"]
}

path "gcp/key/tst-key" {
  capabilities = ["read"]
}

path "gcp/key/stg-key" {
  capabilities = ["read"]
}

path "postgres/creds/dev*" {
  capabilities = ["read"]
}

path "postgres/creds/tst*" {
  capabilities = ["read"]
}

path "postgres/creds/stg*" {
  capabilities = ["read"]
}

path "sys/revoke/*" {
  capabilities = ["update", "list", "sudo"]
}

path "sys/leases/*" {
  capabilities = ["update", "list", "sudo"]
}
