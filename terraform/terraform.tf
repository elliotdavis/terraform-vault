terraform {
  backend "gcs" {
    bucket = "ltf-terraform-state-6ec0"
    prefix = "vault-terraform"
  }
}

provider "vault" {
  address = var.vault_addr
  token   = var.vault_token
}
