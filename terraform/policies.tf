resource "vault_policy" "acq_data_pipeline_function" {
  name   = "acq-data-pipeline-function"
  policy = file("./policies/acq-data-pipeline-function.hcl")
}

resource "vault_policy" "admin" {
  name   = "admin"
  policy = file("./policies/admin.hcl")
}

resource "vault_policy" "approle" {
  name   = "approle"
  policy = file("./policies/approle.hcl")
}

resource "vault_policy" "atlantis_access" {
  name   = "atlantis-access"
  policy = file("./policies/atlantis-access.hcl")
}

resource "vault_policy" "auto_sec_vault" {
  name   = "auto-sec-vault"
  policy = file("./policies/auto-sec-vault.hcl")
}

resource "vault_policy" "certbot" {
  name   = "certbot"
  policy = file("./policies/certbot.hcl")
}

resource "vault_policy" "default" {
  name   = "default"
  policy = file("./policies/default.hcl")
}

resource "vault_policy" "eng" {
  name   = "eng"
  policy = file("./policies/eng.hcl")
}

resource "vault_policy" "eng_mgr" {
  name   = "eng-mgr"
  policy = file("./policies/eng-mgr.hcl")
}

resource "vault_policy" "eng_fe" {
  name   = "eng-fe"
  policy = file("./policies/eng-fe.hcl")
}

resource "vault_policy" "eng_be" {
  name   = "eng-be"
  policy = file("./policies/eng-be.hcl")
}

resource "vault_policy" "github_auto_eng" {
  name   = "github-auto-eng"
  policy = file("./policies/github-auto-eng.hcl")
}

resource "vault_policy" "goldfish" {
  name   = "goldfish"
  policy = file("./policies/goldfish.hcl")
}

resource "vault_policy" "haproxy" {
  name   = "haproxy"
  policy = file("./policies/haproxy.hcl")
}

resource "vault_policy" "jenkins" {
  name   = "jenkins"
  policy = file("./policies/jenkins.hcl")
}

resource "vault_policy" "pritunl" {
  name   = "pritunl"
  policy = file("./policies/pritunl.hcl")
}

resource "vault_policy" "provisioner_policy" {
  name   = "provisioner-policy"
  policy = file("./policies/provisioner-policy.hcl")
}

resource "vault_policy" "qa" {
  name   = "qa"
  policy = file("./policies/qa.hcl")
}

resource "vault_policy" "salt-role" {
  name   = "salt-role"
  policy = file("./policies/salt-role.hcl")
}

resource "vault_policy" "sre-jr" {
  name   = "sre-jr"
  policy = file("./policies/sre-jr.hcl")
}

resource "vault_policy" "sre-sr" {
  name   = "sre-sr"
  policy = file("./policies/sre-sr.hcl")
}

resource "vault_policy" "sysops" {
  name   = "sysops"
  policy = file("./policies/sysops.hcl")
}

## User Policies ##
resource "vault_policy" "sre-ainsua" {
  name   = "sre-ainsua"
  policy = file("./policies/users/sre-ainsua.hcl")
}

resource "vault_policy" "sre-bmajeska" {
  name   = "sre-bmajeska"
  policy = file("./policies/users/sre-bmajeska.hcl")
}

resource "vault_policy" "sre-edavis" {
  name   = "sre-edavis"
  policy = file("./policies/users/sre-edavis.hcl")
}

resource "vault_policy" "sre-jyanko" {
  name   = "sre-jyanko"
  policy = file("./policies/users/sre-jyanko.hcl")
}

resource "vault_policy" "sre-tgeery" {
  name   = "sre-tgeery"
  policy = file("./policies/users/sre-tgeery.hcl")
}

resource "vault_policy" "sre-tpauro" {
  name   = "sre-tpauro"
  policy = file("./policies/users/sre-tpauro.hcl")
}

resource "vault_policy" "sre_1592" {
  count  = var.env == "-dev" ? 1 : 0
  name   = "sre-1592"
  policy = file("./policies/sre-1592.hcl")
}
