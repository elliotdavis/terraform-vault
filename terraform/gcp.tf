resource "vault_gcp_auth_backend" "gcp" {
  credentials = file("keys/vault-admin.json")
}

resource "vault_gcp_secret_backend" "gcproleset_backend" {
  credentials               = file("keys/vault-admin.json")
  path                      = "gcp"
  default_lease_ttl_seconds = "68400"
  max_lease_ttl_seconds     = "68400"
}

resource "vault_gcp_auth_backend_role" "acq_plt" {
  role    = "acq-plt"
  type    = "iam"
  backend = vault_gcp_auth_backend.gcp.path
  bound_service_accounts = [
    "acq-plt@ltf-dev-8366.iam.gserviceaccount.com",
    "acq-plt@ltf-tst-5ca2.iam.gserviceaccount.com",
    "acq-plt@ltf-stg-f22d.iam.gserviceaccount.com",
    "acq-plt@ltf-prd-e3a6.iam.gserviceaccount.com"
  ]
  token_policies = [vault_policy.github_auto_eng.name]
}

resource "vault_gcp_auth_backend_role" "ltf_sre_gce" {
  role           = "ltf-sre-gce"
  type           = "gce"
  backend        = vault_gcp_auth_backend.gcp.path
  bound_projects = ["ltf-sre-2c77"]
  bound_labels   = ["env_name:sre", "env:sre"]
  bound_regions  = ["us-west1", "us-west2", "us-central1"]
  token_policies = [vault_policy.certbot.name, vault_policy.atlantis_access.name]
}

resource "vault_gcp_auth_backend_role" "ltf_jenkins_gce" {
  role           = "ltf-jenkins-gce"
  type           = "gce"
  backend        = vault_gcp_auth_backend.gcp.path
  bound_projects = ["ltf-prd-e3a6"]
  bound_labels   = ["svc:jenkins"]
  bound_regions  = ["us-west1"]
  token_policies = [vault_policy.jenkins.name]
}
resource "vault_gcp_auth_backend_role" "ltf_haproxy_gce" {
  role           = "ltf-haproxy-gce"
  type           = "gce"
  backend        = vault_gcp_auth_backend.gcp.path
  bound_projects = ["ltf-prd-e3a6", "ltf-dev-8366", "ltf-tst-5ca2", "ltf-sre-2c77", "ltf-stg-f22d"]
  bound_labels   = ["svc:haproxy"]
  bound_regions  = ["us-west1", "us-west2", "us-central1"]
  token_policies = [vault_policy.haproxy.name]
}
resource "vault_gcp_auth_backend_role" "ltf_pritunl_gce" {
  role           = "ltf-pritunl-gce"
  type           = "gce"
  backend        = vault_gcp_auth_backend.gcp.path
  bound_projects = ["ltf-sre-2c77", "apg-prd-f589", "apg-dev-5820"]
  bound_labels   = ["svc:pritunl"]
  bound_regions  = ["us-west1", "us-west2", "us-central1"]
  token_policies = [vault_policy.pritunl.name]
}

resource "vault_gcp_secret_roleset" "atlantis_token" {
  backend      = vault_gcp_secret_backend.gcproleset_backend.path
  roleset      = "atlantis-token"
  secret_type  = "access_token"
  project      = var.admin_project
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.admin_project}"

    roles = [
      "roles/owner",
    ]
  }
  depends_on = [vault_gcp_secret_backend.gcproleset_backend]
}

resource "vault_gcp_secret_roleset" "atlantis_key" {
  backend      = vault_gcp_secret_backend.gcproleset_backend.path
  roleset      = "atlantis-key"
  secret_type  = "service_account_key"
  project      = var.admin_project
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.admin_project}"

    roles = [
      "roles/owner",
    ]
  }
  depends_on = [vault_gcp_secret_backend.gcproleset_backend]
}

resource "vault_gcp_secret_roleset" "tfadmin_key" {
  backend      = vault_gcp_secret_backend.gcproleset_backend.path
  roleset      = "tfadmin-key"
  secret_type  = "service_account_key"
  project      = var.admin_project
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.admin_project}"

    roles = [
      "roles/owner",
    ]
  }
}

resource "vault_gcp_secret_roleset" "sre_keys" {
  for_each     = var.sre_projects
  backend      = vault_gcp_secret_backend.gcproleset_backend.path
  roleset      = "${each.key}-key"
  secret_type  = "service_account_key"
  project      = each.value
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${each.value}"
    roles = [
      "roles/owner",
    ]
  }
  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/lt-host-prd-6ec0"
    roles = [
      "roles/dns.admin",
      "roles/compute.networkUser"
    ]
  }
  binding {
    resource = "buckets/ltf-terraform-state-6ec0"
    roles = [
      "roles/storage.objectAdmin",
    ]
  }
  binding {
    resource = "buckets/ltf-terraform-vault-state"
    roles = [
      "roles/storage.objectAdmin",
    ]
  }
  binding {
    resource = "//cloudresourcemanager.googleapis.com/organizations/896351571276"
    roles = [
      "roles/resourcemanager.organizationAdmin",
    ]
  }
  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/ltf-sre-2c77"
    roles = [
      "roles/owner",
    ]
  }
}

resource "vault_gcp_auth_backend_role" "acq_data_pipeline_function" {
  role    = "acq-data-pipeline-function"
  type    = "iam"
  backend = vault_gcp_auth_backend.gcp.path
  bound_service_accounts = [
    "run-lfs@ltf-eng-mcaraballo-60b8.iam.gserviceaccount.com",
    "data-pipeline-event-listener@ltf-dev-8366.iam.gserviceaccount.com",
    "data-pipeline-event-listener@ltf-tst-5ca2.iam.gserviceaccount.com",
    "data-pipeline-event-listener@ltf-stg-f22d.iam.gserviceaccount.com",
    "data-pipeline-event-listener@ltf-prd-e3a6.iam.gserviceaccount.com"
  ]
  token_policies = [vault_policy.acq_data_pipeline_function.name]
  # This must be an hour otherwise Cloud Functions will fail. Default Expiry for JWT in Google is an hour (not configurable)
  max_jwt_exp = 3600
  token_ttl   = 120
}

resource "vault_gcp_auth_backend_role" "todd_test" {
  count       = var.env == "-dev" ? 1 : 0
  role        = "todd-test"
  type        = "iam"
  backend     = vault_gcp_auth_backend.gcp.path
  max_jwt_exp = 3600
  bound_service_accounts = [
    "app-which-is-not-todd@ltf-eng-tmarkle-0651.iam.gserviceaccount.com",
  ]
  token_policies = [vault_policy.sre_1592[0].name]
}
