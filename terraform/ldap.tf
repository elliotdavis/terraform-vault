resource "vault_ldap_auth_backend" "ldap" {
  path           = "ldap"
  url            = "ldap://stunnel-lb.vault${var.env}.lereta.io:5000"
  userattr       = "uid"
  upndomain      = "lereta.com"
  discoverdn     = false
  deny_null_bind = true
  groupdn        = "ou=Groups,dc=lereta,dc=com"
  groupfilter    = "(|(memberUid={{.Username}})(member={{.UserDN}})(uniqueMember={{.UserDN}}))"
}

resource "vault_ldap_auth_backend_group" "engineering" {
  groupname = "engineering"
  policies  = [vault_policy.eng.name]
  backend   = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_group" "sre_sr" {
  groupname = "sre-sr"
  policies  = [vault_policy.sre-sr.name]
  backend   = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_group" "sre_jr" {
  groupname = "sre-jr"
  policies  = [vault_policy.sre-jr.name]
  backend   = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_group" "sysops" {
  groupname = "sysops"
  policies  = [vault_policy.sysops.name]
  backend   = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_group" "qa" {
  groupname = "qa"
  policies  = [vault_policy.qa.name]
  backend   = vault_ldap_auth_backend.ldap.path
}
