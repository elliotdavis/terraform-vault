# enabling postgres database secrets engine
resource "vault_mount" "db" {
  path = "postgres"
  type = "database"
}
