variable "env" {
  default = ""
}

variable "vault_addr" {
  default = "https://vault.lereta.io/"
}

variable "vault_token" {
  default = ""
}

variable "admin_project" {
  default = "lereta-admin"
}

# May have to be broken out to dev/prd if prod Jenkins moves region
variable "jenkins_nat" {
  description = "Cloud NAT IPs for Jenkins Instances"
  type        = list(string)
  default     = ["35.199.183.216/32", "35.197.43.194/32"]
}

variable "salt_nat" {
  type    = list(string)
  default = ["198.204.115.201/32", "35.199.183.216/32", "35.197.43.194/32", "35.235.116.121/32", "35.235.103.168/32", "35.222.222.187/32", "35.193.146.252/32"]
}

variable "sre_projects" {
  description = "map of users/project IDs"
  default = {
    "tgeery"   = "ltf-sre-tgeery-6cd7"
    "edavis"   = "ltf-sre-edavis-6c29"
    "bmajeska" = "ltf-sre-bmajeska-1ee7"
    "jyanko"   = "ltf-sre-jyanko-3c4f"
    "ainsua"   = "ltf-sre-ainsua-4040"
    "tpauro"   = "ltf-sre-tpauro-2250"
    "dev"      = "ltf-dev-8366"
    "tst"      = "ltf-tst-5ca2"
    "stg"      = "ltf-stg-f22d"
    "prd"      = "ltf-prd-e3a6"
    "sre"      = "ltf-sre-2c77"
    "apg-prd"  = "apg-prd-f589"
    "apg-dev"  = "apg-dev-5820"
  }
}
