output "postgres_vault_mount_path" {
  value = vault_mount.db.path
}

output "atlantis_token_serviceaccount" {
  value       = vault_gcp_secret_roleset.atlantis_token.service_account_email
  description = "Atlantis roleset service account email address for bindings"
}

output "atlantis_key_serviceaccount" {
  value       = vault_gcp_secret_roleset.atlantis_key.service_account_email
  description = "Atlantis roleset service account email address for bindings"
}

output "jenkins_role_id" {
  value = vault_approle_auth_backend_role.jenkins_role.role_id
}

output "tf_key_serviceaccount" {
  value = vault_gcp_secret_roleset.tfadmin_key.service_account_email
}

output "tgeery_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["tgeery"].service_account_email
}

output "edavis_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["edavis"].service_account_email
}

output "bmajeska_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["bmajeska"].service_account_email
}

output "jyanko_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["jyanko"].service_account_email
}

output "ainsua_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["ainsua"].service_account_email
}

output "tpauro_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["tpauro"].service_account_email
}

output "dev_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["dev"].service_account_email
}

output "tst_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["tst"].service_account_email
}

output "stg_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["stg"].service_account_email
}

output "prd_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["prd"].service_account_email
}

output "sre_key_service_account" {
  value = vault_gcp_secret_roleset.sre_keys["sre"].service_account_email
}
