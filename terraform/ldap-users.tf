resource "vault_ldap_auth_backend_user" "gjarmstrong" {
  username = "gjarmstrong"
  policies = [vault_policy.eng.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "jstraw" {
  username = "jstraw"
  policies = [vault_policy.eng.name, vault_policy.eng_mgr.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "tgeery" {
  username = "tgeery"
  policies = [vault_policy.admin.name, vault_policy.sre-tgeery.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "edavis" {
  username = "edavis"
  policies = [vault_policy.admin.name, vault_policy.sre-edavis.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "jyanko" {
  username = "jyanko"
  policies = [vault_policy.sre-jr.name, vault_policy.sre-jyanko.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "bmajeska" {
  username = "bmajeska"
  policies = [vault_policy.admin.name, vault_policy.sre-sr.name, vault_policy.sre-bmajeska.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "ainsua" {
  username = "ainsua"
  policies = [vault_policy.sre-jr.name, vault_policy.sre-ainsua.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "tpauro" {
  username = "tpauro"
  policies = [vault_policy.sre-jr.name, vault_policy.sre-tpauro.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "mmill" {
  username = "mmill"
  policies = [vault_policy.sysops.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "dstooksberry" {
  username = "dstooksberry"
  policies = [vault_policy.eng.name, vault_policy.sysops.name, vault_policy.eng_fe.name, vault_policy.eng_be.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "tmarkle" {
  username = "tmarkle"
  policies = [vault_policy.eng.name, vault_policy.eng_be.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "rsenar" {
  username = "rsenar"
  policies = [vault_policy.eng.name, vault_policy.eng_be.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "mcaraballo" {
  username = "mcaraballo"
  policies = [vault_policy.eng.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "bzaugg" {
  username = "bzaugg"
  policies = [vault_policy.eng.name]
  backend  = vault_ldap_auth_backend.ldap.path
}

resource "vault_ldap_auth_backend_user" "kyedla" {
  username = "kyedla"
  policies = [vault_policy.qa.name]
  backend  = vault_ldap_auth_backend.ldap.path
}
