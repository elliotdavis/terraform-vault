variable "client_secret" {
  description = "Required: OIDC Client secret for Hashicorp Vault"
  type        = string
}

variable "client_id" {
  description = "Required: OIDC Client ID for Hashicorp Vault"
}

variable "discovery_url" {
  description = "OIDC Discovery endpoint"
  type        = string
  default     = "http://address:8080/realms/itreachery"
}

variable "authorized_redirects" {
  description = "List of authorized redirects for Okta OIDC"
  type        = list(string)
  default     = ["http://127.0.0.1:8250/oidc/oidc/callback", "http://address:8200/ui/vault/auth/oidc/oidc/callback"]
}
