# Enabling github backend for vault
resource "vault_github_auth_backend" "github_lereta" {
  organization = "LERETA"
  description  = "Vault github backend to enable github private auth tokens"
}

resource "vault_github_user" "auto_eng" {
  backend  = vault_github_auth_backend.github_lereta.id
  user     = "auto-eng"
  policies = [vault_policy.github_auto_eng.name]
}

