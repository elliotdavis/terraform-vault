# terraform-vault changelog
## Febuary 24,2022
* [not ticket assiogned]
  * added TF to enable OIDC/SSO with keycloak and group assignent.

## Febuary 10, 2020
* [SRE-1753](https://jira.lereta.net/browse/SRE-1753)
    * creating eng/ secret v2 backend
    * creating eng-be and eng-fe groups/policies for engineering
    * renaming engineering policy to eng
    * preventing deletes from secret/ and eng/ for eng policy

## Febuary 5, 2020
* [SRE-1762](https://jira.lereta.net/browse/SRE-1762)
    * bound apg-prd and apg-dev projects to pritunl role set

## January 24, 2019
* removing read access from dev_user credentials in secret/databases/dev/dev_user

## January 23, 2020
* [SRE-1696](https://jira.lereta.net/browse/SRE-1696)
    * adding APG Projects to GCP Rolesets
    * updating approle policy with sre/ pathing

## January 14, 2020
* [SRE-1668](https://jira.lereta.net/browse/SRE-1668)
    * Created QA Policy
    * Created QA LDAP Group
    * Created kyedla ldap user
    * Modified engineering policy to block stg/prd database credentials

## January 07, 2020
* [SRE-1592](https://jira.lereta.net/browse/SRE-1592)
    * updating JWT expiry for todd_test gcp auth role
    * updating path for sre-1592 policy

## January 02, 2020
* [SRE-1592](https://jira.lereta.net/browse/SRE-1592)
    * adding test role for Todd in DEV Vault Cluster
    * adding test policy for Todd in DEV Vault Cluster
    * cleaning up duplicate policy in admin
    * adding `secret/test` secrets to terraform setup - needed for test jobs

## December 17, 2019
* [SRE-1456](https://jira.lereta.net/browse/SRE-1456)
    * updating sre-jr and sre-sr policies with lease permissions (needed for service account key rotator)

## December 16, 2019
* [SRE-1456](https://jira.lereta.net/browse/SRE-1456)
    * revoking engineering policy to restrict access to sub directories
    * moving sysops mount to secrets-backends file
    * creating sre/ secret v2 mount point
    * adding new sre/ path to admin policy
    * moving gcp roleset:
        * OLD: `vault read gcproleset-sre-tgeery-key/key/sre-tgeery-key`
        * NEW: `vault read gcp/key/tgeery-key`
    * updating policies with new gcp roleset pathing
    * removing vault-cf test backend role
    * removing google provider since we're not managing google resources anymore
    * modifying kv secrets version to match with PRD
* TODO: Import `gcpkms` backend (waiting for provider)

## December 11, 2019
* [SRE-1542](https://jira.lereta.net/browse/SRE-1542) Fix jenkins production approle name to conform to 3 letter env

## December 10, 2019
* [SRE-1529](https://jira.lereta.net/browse/SRE-1529) - modified approle policy for jenkins
    * Created jenkins approle for dev
    * Created jenkins approle for prd
    * Limited secret_id usage to west1 CloudNAT CIDR
    * Restricted token TTL to 30 minutes
    * Limted token usage to 2 uses

## December 9, 2019
* adding Byran to ldap users
* adding approle policy to jenkins approle

## December 5, 2019
* [SRE-1516](https://jira.lereta.net/browse/SRE-1516) - adding sys postgres path to github-auto-eng policy
* updaitng admin policy with secret/ and sysops/ paths

## November 27, 2019
* [SRE-1456](https://jira.lereta.net/browse/SRE-1456)
    * moving individual policies to a policies folder and contained in policies.tf

## November 21, 2019
* [SRE-1489](https://jira.lereta.net/browse/SRE-1489)
    * Modified IAM policy to support org level IAM access

## November 18, 2019
* [SRE-1452](https://jira.lereta.net/browse/SRE-1452)
    * adding acq-data-pipeline-function role
    * adding acq-data-pipeline-function policy
    * removing tmarkle's test role/policy
    * adding mcaraballo to ldap-users

## November 11, 2019
* [SRE-1446](https://jira.lereta.net/browse/SRE-1446) - creating LDAP User binding for dstooksberry

## November 8, 2019
* [SRE-1438](https://jira.lereta.net/browse/SRE-1438)
    * creating sysops secret backend v2
    * creating sysops policy
    * adding mmill to ldap users
    * created sysops ldap group permissions

## October 31, 2019
* [SRE-1403](https://jira.lereta.net/browse/SRE-1403) - Re-write vault SRE ENV service account keys to use member vs binding.

## October 24, 2019
* [SRE-1375](https://jira.lereta.net/browse/SRE-1375) - Modifying tpauro policy to remove prd env

## October 23, 2019
* [SRE-1361](https://jira.lereta.net/browse/SRE-1361) - Created GCP rolesets added remainder of SRE team to gcprolesets

## October 22, 2019
* [SRE-1356](https://jira.lereta.net/browse/SRE-1356) - Created GCP rolesets for tpauro and ainsua private service projects

## October 17, 2019
* [SRE-1337](https://jira.lereta.net/browse/SRE-1337) - Fixed gcproleset service account key permissions

## October 17, 2019
* [SRE-1336](https://jira.lereta.net/browse/SRE-1336) - Added AInsua to ldap-users policy

## October 16, 2019
* [SRE-1335](https://jira.lereta.net/browse/SRE-1335) - Added Brian to ldap-user service account keys policy

## October 14, 2019
* [SRE-1327](https://jira.lereta.net/browse/SRE-1327) - fixing policy for dependency checker job in Jenkins DEV
    * also updating a deprecated argument

## October 10, 2019
* [SRE-1312](https://jira.lereta.net/browse/SRE-1312) - create ENV group in policies for access to SRE project keys (vault can now manage dynamically manage the service account key)

## October 8, 2019
* [SRE-1301](https://jira.lereta.net/browse/SRE-1301) create SR group in policy for access to tfadmin key (vault can now manage dynamically manage the service account key)

## October 7, 2019
* [SRE-1252](https://jira.lereta.net/browse/SRE-1252) - added GCP ROLESET for master terraform admin key 24 hour life
* [SRE-1288](https://jira.lereta.net/browse/SRE-1288) - updating github-autoeng policy for secrets backend v2

## October 3, 2019
* [SRE-1277](https://jira.lereta.net/browse/SRE-1277) - adding imported approle auth backend and defining one for Jenkins
* [SRE-1236](https://jira.lereta.net/browse/SRE-1236) - updating github-auto-eng policy for secret/ backend V2 and Approle Secret TTL to 0

## September 30, 2019
* [SRE-1263](https://jira.lereta.net/browse/SRE-1263) - added GCP roleset for atlantis.  Atlantis no longer uses service account key file

## September 17, 2019
* adding test path for Jenkins Test Job

## September 16, 2019
* updating permissions for engineering policy
* adding a few users to ldap-users

## September 11, 2019
* [SRE-1194](https://jira.lereta.net/browse/SRE-1194) - added gcpkms backend and dev policy

## August 28, 2019
* [SRE-1174](https://jira.lereta.net/browse/SRE-1174)
    * Changed config for HA proxy policy

## August 27, 2019
* [SRE-1155](https://jira.lereta.net/browse/SRE-1155)
    * creating test role and policy for Todd
* Securing secet/[service_accounts, dns, sre, databases] paths from engineering

## August 26, 2019
* [SRE-1140](https://jira.lereta.net/browse/SRE-1140)
    * adding env setup for stg and prd

## August 19, 2019
* adding new policy for pritunl

## August 13, 2019
* adding new policy for haproxy

## August 13, 2019
* vault 1.2.1 changes

## August 1, 2019
* adding new policy/role for jenkins
* adding tst account for acq-plt role

## July 26, 2019
* adding auth backend for jenkins

## July 9, 2019
* terraform 0.12 compatibility

## June 6, 2019
* updating developer policy name to reflect auto-eng github user
* adding github backend to terraform code
* adding auto-eng github user to terraform code

## June 6, 2019
* added Atlassian polices for the 4 environments
* Imported the github bakend

## May 14, 2019
* adding workspace variables for dev
* adding postgres db mount point

## May 1, 2019
### Added
* Adding LDAP Configuration
    * Basic LDAP Configuration
    * SRE Group/Policies
* imported the following policies
    * admin
    * approle
    * atlantis-access
    * certbot
    * default
    * goldfish
    * provisioner-policy
